//入口函数 main
//基本类型 int double bool list set map null
//类型可推导
//var const final 区别
//函数 可选参数 命名参数

printName(String name, int age, [String xb = '男']) {
  print("${name}---${age}--${xb}");
}

printName2({name = 'tangguodalong', age, xb}) {
  print("${name}---${age}--${xb}");
}

void main(List<String> arguments) {
  // print("Hello world! ${arguments}");
  /** 
  int age = 28;
  double amount = 10.0;
  bool flag = true;
  var list = [1, 2, 2, 2, 3, 4, 5, 'tangguodalong'];
  Set set = new Set();
  set.addAll(list);
  print(set);
  print(list);
  var m = {'name': 'tangguodalong', 'age': 28};
  print("my name is ${m['name']}");

  //const age;
  final age1;
  age1 = 28;
  print(age1);
  //print(age);
    */
  //printName('tangguodalong', 28, '未知');
  printName2(
    age: 28,
    xb: '男',
    name: 'zhangsan',
  );
}
