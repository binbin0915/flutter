printNameLenght(String? name) {
  if (name != null) {
    print('${name}-- ${name!.length}');
  }
  print('${name}-- ${name?.length}');
}

//运行时才发现
//空安全null safety null 可见 可控
//dart如何实现 ? 可空类型 int? double? String?
//String? ==== String|null
//?. 成员访问符 如果为空不调用属性或方法
//断言 ！
//late 明确在后面赋值
void main() {
  printNameLenght('tangguodalong');
  //NoSuchMethodError: The getter 'length' was called on null.
  printNameLenght(null);
  List list = <String?>['Simba', 'Jerry', 'Tom', null];
  list.forEach((element) {
    printNameLenght(element);
  });
}
