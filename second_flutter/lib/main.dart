import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
            child: ConstrainedBox(
          constraints:
              BoxConstraints(minWidth: 100, minHeight: 100, maxWidth: 300),
          child: Container(
            width: 400,
            height: 400,
            decoration: BoxDecoration(color: Colors.blue),
            child: Text('tangguodalong'),
          ),
        )),
      ),
    );
  }
}

class AlignWidget extends StatelessWidget {
  const AlignWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          'Tangguodalong',
          style: TextStyle(color: Colors.blue, fontSize: 20),
        ),
      ),
    );
  }
}

class ContainnerWidget extends StatelessWidget {
  const ContainnerWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        //Cannot provide both a color and a decoration
        width: 400,
        height: 400,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.blue,
            border: Border.all(
              color: Colors.red,
              width: 5,
            )),
        alignment: Alignment.center,
        child: Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(
                color: Colors.red, width: 5, style: BorderStyle.solid),
            color: Colors.yellow,
          ),
        ),
      ),
    );
  }
}
